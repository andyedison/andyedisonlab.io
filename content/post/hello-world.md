---
title: "Hello World"
subtitle: "First Post"
tags: ["hello-world"]
date: 2018-01-20
---

This is my first post! This new blog was built with the very cool [Hugo](https://gohugo.io/) and hosted on [Gitlab Pages](https://gitlab.com/pages/hugo)! 

More details and likely a quick post about setting it up coming soon.