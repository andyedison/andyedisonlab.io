---
title: About me
comments: false
---

My name is Andy Edison. I'm a just a guy trying to share my passion for the world with anyone who will listen.

Some of my favorite things in no particular order:

- Being a twin Dad
- All things tech
- Go/Golang
- Bourbon
- BBQ Chef
- Go [Chiefs](https://en.wikipedia.org/wiki/Kansas_City_Chiefs)! and Go [Royals](https://en.wikipedia.org/wiki/Kansas_City_Royals)!
- Weight lifting
- Hunting, fishing, hiking, etc. Love the outdoors


### On this site

You're likely to find posts about whatever is tickling my fancy at the time. Today it might be golang and creating this blog with Hugo, tomorrow it could be the tasting notes on this killer bottle of Buffalo Trace that my local grocery store had hand picked for them.

### Contact me

Hit me up on [Twitter](https://twitter.com/andyedison) and let me know what's on your mind.
